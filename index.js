//ex 3
function ex3() {
  var n = document.getElementById("txt-num-n-3")?.value * 1;
  //   var n = 20;
  var temp = "";
  for (let i = 2; i <= n; i++) {
    var flag = true;
    for (let x = 2; x < i; x++) {
      if (i % x === 0) {
        flag = false;
      }
      if (!flag) {
        break;
      }
    }
    if (flag) {
      temp += i + " ";
      console.log({ temp });
    }
  }

  document.getElementById("result3").innerHTML = `Các số nguyên tố: ${temp}`;
}

document.getElementById("btn-ex3").addEventListener("click", ex3);
